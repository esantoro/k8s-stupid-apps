FROM python:3.7-alpine

RUN adduser --home /webapp --uid 2000 --disabled-password www-data
RUN pip3 install flask

RUN mkdir -p /webapp/templates
COPY templates/env.html /webapp/templates/env.html
COPY app.py /webapp/app.py
RUN chown -R www-data:www-data /webapp

WORKDIR /webapp

USER www-data

EXPOSE 5000
ENV FLASK_APP=app.py
ENTRYPOINT python3 app.py