#!/usr/bin/python3
from flask import *

import os

app = Flask(__name__)

@app.route("/ready")
def ready():
    return "OK"

@app.route("/")
def hello():
    return "Hello, world!"

@app.route("/env")
def env():
    data = {key:os.environ[key] for key in os.environ.keys()}
    return render_template("env.html",data=data)

if __name__ == "__main__" :
    app.run(debug=True, host="0.0.0.0")
